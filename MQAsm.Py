#
#Imports
import os
import platform
import random
import sys
#
#General helper functions
def enumerationOf(className, *argsList):
    return type(className,(),dict(zip(argsList, range(len(argsList)))))
def tokVals(tokenObj):
        if tokenObj.tokTyp == tokenTypes.opCode:
                return opNames[tokenObj.tokVal]
        elif tokenObj.tokTyp == tokenTypes.registerName:
                return '#'+tokenObj.tokVal[1:-1]
        elif tokenObj.tokTyp == tokenTypes.sectionName:
                return '@'+globVars.sectionNames[tokenObj.tokVal]
        elif tokenObj.tokTyp == tokenTypes.labelName:
                return tokenObj.tokVal[3:]
        else:
                return tokenObj.tokVal
#
#Global variables
class validOpts:
        platForms=('Linux',)
        labelChars=('ABCDEFGHIJKLMNOPQRSTUVWXYZ_','1234567890')
tokTyps=(
        'bitsArr',
        'stringLiteral',
        'opCode',
        'registerName',
        'sectionName',
        'labelName',
        'genericUnknown'
        )
tokenTypes=enumerationOf('tokenType',*tokTyps)
class globVars:
        opCodes={#Ensure everything here is a tuple- apart from the keys, obviously.
                'NP':tuple(),
                'IT':tuple(),
                'AD':(
                        (tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'FP':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'GZ':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        (tokenTypes.genericUnknown,),
                        ),
                'CP':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        (tokenTypes.registerName,),
                        ),
                'RE':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        (tokenTypes.registerName,),
                        ),
                'WR':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'LS':(
                        (tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'RS':(
                        (tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'AN':(
                        (tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'OR':(
                        (tokenTypes.registerName,),
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        ),
                'RL':(
                        (tokenTypes.genericUnknown,),
                        (tokenTypes.registerName,),
                        ),
                'AA':(
                        (tokenTypes.bitsArr,tokenTypes.registerName,),
                        (tokenTypes.registerName,),
                        ),
                }
        registerNames=(
                'N0',
                'N1',
                'S0',
                'S1',
                'T0',
                'T1',
                'T2'
                )
        sectionNames=(
                'Body',
                'Sized',
                'Valued'
                )
        namedLabels=[]
        hasBody=False
        bodyLabels=[]
        headLabels=[]
        arrLabels=[]
        jumpLabels=[]
class compOpts:
        entryPoint='Entry'
        heapSize=256
        outputName='outPut'
        platForm=platform.system()
        compFlags={
                '-R':False,
                '-T':False
                }
opNames=tuple([opCodeName for opCodeName in globVars.opCodes.keys()])
opCodes=enumerationOf('opCode',*globVars.opCodes)
sectionNames=enumerationOf('sectionName',*globVars.sectionNames)
#
#Structures
class tokenT:
        def __init__(self,tokenValue,tokenType):
                self.tokVal = tokenValue
                self.tokTyp = tokenType
class opT:
        def __init__(self,opCode,*argsList):
                self.opCode = opCode
                self.argsList = argsList
class labelT:
        def __init__(self,labelName):
                self.labelName = labelName
class arrT:
        def __init__(self,labelName,arrayValues):
                self.labelName = labelName
                self.arrayValues = arrayValues
                self.arraySize = len(arrayValues)
#
#Commandline outputting
def printError(errorString):
        print'\033[91mError\033[00m: {}'.format(errorString)
        exit(1)
def printErrorOnLine(errorString, lineNum, fileName):
        print'\033[91mError \033[00mon line {} of {}: {}'.format(lineNum, fileName, errorString)
        exit(1)
def printWarning(warningString):
        print'\033[93mWarning\033[00m: {}'.format(warningString)
def printWarningOnLine(warningString, lineNum, fileName):
        print'\033[93mWarning \033[00mon line {} of {}: {}'.format(lineNum, fileName, warningString)
#
#Parameter checking
def checkLabelValidity(labelName):
        if len(labelName) < 1:
                return False
        elif labelName[0] not in validOpts.labelChars[0]:
                return False
        for currChar in labelName[1:]:
                if currChar.upper() not in ''.join(validOpts.labelChars):
                        return False
        return True
def checkIntegerRange(inNum,bitLength=64):
        try:
                inNum = int(inNum,10)
        except ValueError:
                return 0
        else:
                if inNum < 0 or inNum >= 2**bitLength:
                        return 1
                else:
                        return 2
#
#Core functions
def parseArgs(argList):
        inputFiles = []
        for currArg in argList:
                if currArg.upper()[:2]=='-E':
                        labelName = currArg[2:]
                        if checkLabelValidity(labelName) == False:
                                printError('Invalid entry point label name \'{}\'. Names must follow specifications listed on documentation.'.format(labelName))
                        compOpts.entryPoint = labelName
                elif currArg.upper()=='-H':
                        print'-E(labelName) : Sets the entry point for the program. Must be a valid label, defaults to \'{}\'.'.format(compOpts.entryPoint)
                        print'-H            : Prints a list of command line arguments and what they do.'
                        print'-M(heapSize)  : Sets the amount of allocated memory for the program in bytes, defaults to \'{}\'.'.format(compOpts.heapSize)
                        print'-O(fileName)  : Determines output file name, defaults to \'{}\'.'.format(compOpts.outputName)
                        print'-P(platForm)  : Sets the target platform for compilation. Valid inputs are (\'{}\'), defaults to \'{}\'.'.format('\',\''.join(validOpts.platForms),compOpts.platForm)
                        print'-R            : Runs the file after compilation.'
                        print'-T            : Preserves the translated files, rather than attempting to compile it.'
                        exit()
                elif currArg.upper()[:2]=='-M':
                        inNum = currArg[2:]
                        if checkIntegerRange(inNum) == 0:
                                printError('Invalid heap size \'{}\'. Value must be a positive 64-bit integer.'.format(inNum))
                        elif checkIntegerRange(inNum) == 1:
                                printWarning('Heap size \'{}\' is out of 64-bit range. Bit roll-over will be present. Unexpected behaviour may occur.'.format(inNum))
                        compOpts.heapSize = int(inNum,10) & (2**64-1)
                elif currArg.upper()[:2]=='-O':
                        outName = currArg[2:]
                        if len(outName) == 0:
                                printError('Invalid output file name \'{}\'.'.format(outName))
                        compOpts.outputName = outName
                elif currArg.upper()[:2]=='-P':
                        platForm = currArg[2:]
                        if len(platForm) == 0 or platForm not in validOpts.platForms:
                                printError('Invalid target platform \'{}\'. Valid inputs are (\'{}\').'.format(platForm,'\',\''.join(validOpts.platForms)))
                        compOpts.platForm = platForm
                elif currArg.upper() in compOpts.compFlags.keys():
                        compOpts.compFlags[currArg] = True
                else:
                        try:
                                open(currArg, 'r').close()
                        except IOError:
                                printError('File \'{}\' could not be found in the given directory.'.format(currArg))
                        else:
                                inputFiles.append(currArg)
        if len(inputFiles)==0:
                printError('No valid input files found. At least one input file must be given.'.format(sys.argv[0]))
        else:
                return inputFiles
def parseFiles(inputFiles):
        fileCode = {}
        for currFileName in inputFiles:
                fileCode[currFileName] = []
                currFile = open(currFileName,'r')
                for currLine in currFile.readlines():
                        fileCode[currFileName].append(currLine[:-2] if currLine[-2:] == '\r\n' else currLine[:-1] if currLine[-1:] == '\r' or currLine[-1:] == '\n' else currLine)
                currFile.close()
        return fileCode
def lexFiles(codeFromFiles):
        tokenList = {}
        for currFile in codeFromFiles.keys():
                tokenList[currFile] = lexFile(codeFromFiles[currFile],currFile)
        return tokenList
def lexFile(fileCode, fileName):
        tokenList = [None]*len(fileCode)
        for lineNum, currLine in enumerate(fileCode):
               try:
                       tokenList[lineNum] = tokenizeLine(currLine)
               except Exception as errorText:
                       printErrorOnLine(str(errorText)+'\n{}:\'{}\''.format(lineNum+1,currLine),lineNum+1,fileName)
        return tokenList
def tokenizeLine(lineCode):
        lineTokens = []
        if '\'' not in lineCode:
                lineCode = lineCode.split(';')[0]
        else:
                if (lineCode.count('\'') - lineCode.count('\\\'')) % 2:
                        raise Exception('Closing apostrophe could not be found in string literal.')
        skipCount = 0
        lineCode = lineCode.split(' ')
        for currIdx,currWord in enumerate(lineCode):
                if skipCount != 0:
                        skipCount -= 1
                        continue
                elif len(currWord) == 0:
                        continue
                elif currWord[0] == '\'':
                        if len(currWord) != 1 and currWord[-1] == '\'' and currWord[-2] != '\\':
                                try:
                                        currToken=tokenT(ord(currWord[1:-1])&(2**64-1),tokenTypes.bitsArr)
                                except TypeError:
                                        currToken=tokenT(currWord[1:-1],tokenTypes.stringLiteral)
                        else:
                                tokenValue = currWord[1:]
                                while True:
                                        skipCount += 1
                                        nextWord = lineCode[skipCount+currIdx]
                                        if len(nextWord) != 0 and nextWord[-1] == '\'' and (len(nextWord)==1 or nextWord[-2] != '\\'):
                                                tokenValue += ' ' + nextWord[:-1]
                                                break
                                        else:
                                                tokenValue += ' ' + nextWord
                                currToken=tokenT(tokenValue,tokenTypes.stringLiteral)
                elif currWord in globVars.opCodes.keys():
                        currToken=tokenT(opCodes.__dict__[currWord],tokenTypes.opCode)
                elif currWord[0] == '#':
                        if currWord[1:] not in globVars.registerNames:
                                raise Exception('Unknown register \'{}\'.'.format(currWord))
                        else:
                                currToken=tokenT('[{}]'.format(currWord[1:]),tokenTypes.registerName)
                elif currWord[0] == '@':
                        if currWord[1:] not in globVars.sectionNames:
                                raise Exception('Unknown section \'{}\'.'.format(currWord))
                        else:
                                currToken=tokenT(sectionNames.__dict__[currWord[1:]],tokenTypes.sectionName)
                elif currWord[0] == ';':
                        break
                elif currWord[-1] == ':':
                        if checkLabelValidity(currWord[:-1]) == False:
                                raise Exception('Invalid label name \'{}\'. Label names must follow specifications listed on documentation.'.format(currWord[:-1]))
                        elif currWord[:-1] not in globVars.opCodes.keys():
                                if currWord[:-1] not in globVars.namedLabels:
                                        globVars.namedLabels.append('MQ_'+currWord[:-1])
                                        currToken=tokenT('MQ_'+currWord[:-1],tokenTypes.labelName)
                                else:
                                        raise Exception('Already named label \'{}\' in another location.'.format(currWord[:-1]))
                        else:
                                raise Exception('Invalid label name \'{}\'. Labels cannot share names with instructions.'.format(currWord[:-1]))
                else:
                        try:
                                if currWord[:3] == '0B0':
                                        numVal = int(currWord[3:],2)
                                elif currWord[:3] == '0O0':
                                        numVal = int(currWord[3:],8)
                                elif currWord[:3] == '0H0':
                                        numVal = int(currWord[3:],16)
                                else:
                                        numVal = int(currWord,10)
                                currToken=tokenT(numVal,tokenTypes.bitsArr)
                        except ValueError:
                                for currChar in currWord:
                                        if currChar.upper() not in ''.join(validOpts.labelChars):
                                                raise Exception('Invalid token \'{}\'.'.format(currWord))
                                currToken=tokenT(currWord,tokenTypes.genericUnknown)
                lineTokens.append(currToken)
        return lineTokens
def parseSyntaxOfFiles(tokenLists,codeFromFiles):
        parsedFiles = {'Body':[],'Head':[]}
        for currFile in tokenLists.keys():
                parsedLines = parseFileSyntax(tokenLists[currFile], codeFromFiles[currFile], currFile)
                for currSect in parsedLines.keys():
                        parsedFiles[currSect] = parsedLines[currSect]+parsedFiles[currSect]
        return parsedFiles
def parseFileSyntax(tokenList, codeLines, fileName):
        if 'MQ_'+compOpts.entryPoint not in globVars.namedLabels:
                printError('Entry point \'{}\' was never declared.'.format(compOpts.entryPoint))
        parsedLines = dict([(currKey,[]) for currKey in globVars.sectionNames])
        fileSect = None
        for lineNum, currLine in enumerate(tokenList):
                try:
                        if len(currLine) < 1:
                                continue
                        elif currLine[0].tokTyp == tokenTypes.sectionName:
                                if len(currLine) != 1:
                                        raise Exception('Unexpected token following data section declaration.')
                                fileSect = currLine[0].tokVal
                        else:
                                if fileSect == sectionNames.Body:
                                        parsedLine = parseLineSyntax_Body(currLine)
                                elif fileSect == sectionNames.Valued:
                                        parsedLine = parseLineSyntax_Valued(currLine)
                                elif fileSect == sectionNames.Sized:
                                        parsedLine = parseLineSyntax_Sized(currLine)
                                else:
                                        raise Exception('No valid data section was given before code was parsed.')
                                for currSect in parsedLine.keys():
                                        parsedLines[currSect] = parsedLines[currSect]+parsedLine[currSect]
                except Exception as errorText:
                        printErrorOnLine(str(errorText)+'\n{}:\'{}\''.format(lineNum+1,codeLines[lineNum]),lineNum+1,fileName)
        return{'Body':parsedLines['Body'],'Head':parsedLines['Sized']+parsedLines['Valued']}
def parseLineSyntax_Body(lineTokens):
        parsedLine = []
        if lineTokens[0].tokTyp == tokenTypes.labelName:
                if len(lineTokens) != 1:
                        raise Exception('No tokens should follow a label declaration in \'@Body\'.')
                else:
                        globVars.bodyLabels.append(lineTokens[0].tokVal)
                        parsedLine.append(labelT(lineTokens[0].tokVal))
        elif lineTokens[0].tokTyp == tokenTypes.opCode:
                opCodeName = tokVals(lineTokens[0])
                opParams = globVars.opCodes[opCodeName]
                if len(lineTokens[1:]) == len(opParams):
                        for currIdx, currToken in enumerate(lineTokens[1:]):
                                if currToken.tokTyp not in tuple(opParams[currIdx]):
                                        raise Exception('Unexpected token in position {} operation \'{}\'. Expected type \'{}\', but got \'{}\' of type \'{}\'.'.format(
                                        str(currIdx+1),opCodeName,'\' or \''.join([tokTyps[currTyp] for currTyp in tuple(opParams[currIdx])]),tokVals(currToken),tokTyps[currToken.tokTyp]))
                                elif opCodeName[0] == 'G' and currToken.tokTyp == tokenTypes.genericUnknown:
                                        currToken.tokVal = 'MQ_'+currToken.tokVal
                                        globVars.jumpLabels.append(currToken.tokVal)
                                elif opCodeName == 'RL' and currToken.tokTyp == tokenTypes.genericUnknown:
                                        currToken.tokVal = 'MQ_'+currToken.tokVal
                                        globVars.arrLabels.append(currToken.tokVal)
                        parsedLine.append(opT(tokVals(lineTokens[0]),*[currToken.tokVal for currToken in lineTokens[1:]]))
                else:
                        raise Exception('Invalid number of arguments for operation \'{}\'. Expected {} but got {}.'.format(opCodeName,len(opParams),len(lineTokens[1:])))
        else:
                raise Exception('Unexpected token \'{}\' of type \'{}\'.'.format(tokVals(lineTokens[0]), tokTyps[lineTokens[0].tokTyp]))
        return{'Body': parsedLine}
def parseLineSyntax_Sized(lineTokens):
        if lineTokens[0].tokTyp == tokenTypes.labelName:
                if len(lineTokens[1:]) == 1:
                        if lineTokens[1].tokTyp == tokenTypes.bitsArr:
                                if lineTokens[1].tokVal > 0:
                                        globVars.headLabels.append(lineTokens[0].tokVal)
                                        return{'Sized':[arrT(lineTokens[0].tokVal,list([random.randint(0, 255) for currIdx in range(lineTokens[1].tokVal)]))]}
                                else:
                                        raise Exception('Cannot make array \'{}\' with size 0.'.format(tokVals(lineTokens[0])))
                        else:
                                raise Exception('Unexpected token \'{}\' of type \'{}\'.'.format(tokVals(lineTokens[0]), tokTyps[lineTokens[0].tokTyp], tokTyps[tokenTypes.bitsArr]))
                else:
                        raise Exception('Expected 1 argument to follow label \'{}\', but got \'{}\'.'.format(tokVals(lineTokens[0]),len(lineTokens[1:])))
        else:
                raise Exception('Unexpected token \'{}\' of type \'{}\'. Expected type \'{}\'.'.format(tokVals(lineTokens[0]), tokTyps[lineTokens[0].tokTyp],tokenTypes.bitsArr))
def parseLineSyntax_Valued(lineTokens):
        arrayValues = []
        if lineTokens[0].tokTyp == tokenTypes.labelName:
                if len(lineTokens[1:]) > 0:
                        for currToken in lineTokens[1:]:
                                if currToken.tokTyp == tokenTypes.bitsArr:
                                        globVars.headLabels.append(lineTokens[0].tokVal)
                                        if currToken.tokVal == 0:
                                                arrayValues.append(currToken.tokVal)
                                        else:
                                                for currIdx in range(8):
                                                        currVal = (currToken.tokVal >> (currIdx * 8))
                                                        if currVal == 0:
                                                                break
                                                        else:
                                                                arrayValues.append(currVal & 0xFF)
                                elif currToken.tokTyp == tokenTypes.stringLiteral:
                                        globVars.headLabels.append(lineTokens[0].tokVal)
                                        arrayValues += [ord(currChar)&(2**8-1) for currChar in currToken.tokVal.decode('string-escape')]
                                else:
                                        raise Exception('Unexpected token \'{}\' of type \'{}\'.'.format(tokVals(lineTokens[0]), tokTyps[lineTokens[0].tokTyp], tokTyps[tokenTypes.bitsArr]))
                else:
                        raise Exception('Expected at least 1 argument to follow label \'{}\'.'.format(tokVals(lineTokens[0])))
        else:
                raise Exception('Unexpected token \'{}\' of type \'{}\'. Expected type \'{}\'.'.format(tokVals(lineTokens[0]), tokTyps[lineTokens[0].tokTyp],tokenTypes.bitsArr))
        return{'Valued':[arrT(lineTokens[0].tokVal,arrayValues)]}
def parseHeap(parsedFiles):
        for currLabel in globVars.jumpLabels:
                if currLabel not in globVars.bodyLabels:
                        printError('Attempted to go to label \'{}\', which is not in \'@Body\'.'.format(currLabel[3:]))
        for currLabel in globVars.arrLabels:
                if currLabel not in globVars.headLabels:
                        printError('Attempted to address label \'{}\', which is not in \'@Sized\' or \'@Valued\'.'.format(currLabel[3:]))
        heapData = []
        currOffset = 0
        heapOffsets = {}
        for currArr in parsedFiles['Head']:
                heapData += currArr.arrayValues
                heapOffsets[currArr.labelName] = currOffset
                currOffset += currArr.arraySize
        if currOffset > compOpts.heapSize:
                printError('Allocated space exceeded heap size. Try resizing using -M{1}.\n        Current size: {0}, Required size: {1}, Difference: {2} bytes.'.format(
                compOpts.heapSize,currOffset,currOffset-compOpts.heapSize))
        heapData += [random.randint(0, 255) for currIdx in range(compOpts.heapSize-currOffset)]
        return{'bodySection':parsedFiles['Body'],'heapData':heapData,'heapOffsets':heapOffsets}
class genLinux:
        platFormName = 'Linux'
        commentChar = ';'
        opExpansion={
                'NP':(
                        'NOP',
                        ),
                'IT':(
                        'MOV RAX,[N0]',
                        'MOV RDI,[N1]',
                        'MOV RSI,[S0]',
                        'MOV RDX,[S1]',
                        'MOV R10,[T0]',
                        'MOV R8,[T1]',
                        'MOV R9,[T2]',
                        'sysCall'
                        ),
                'AD':(
                        'MOV RAX,{0}',
                        'ADD RAX,{1}',
                        'MOV {0},RAX'
                        ),
                'FP':(
                        'MOV RAX,{0}',
                        'NOT RAX',
                        'MOV {0},RAX'
                        ),
                'GZ':(
                        'MOV RAX,{0}',
                        'TEST RAX,RAX',
                        'JZ {1}'
                        ),
                'CP':(
                        'MOV RAX,{0}',
                        'MOV {1},RAX'
                        ),
                'RE':(
                        'MOV RAX,{0}',
                        'ADD RAX,heapArray',
                        'MOV RAX,[RAX]',
                        'MOV {1},RAX'
                        ),
                'WR':(
                        'MOV RBX,{0}',
                        'MOV RAX,{1}',
                        'ADD RAX,heapArray',
                        'MOV [RAX],RBX'
                        ),
                'LS':(
                        'MOV RAX,{0}',
                        'SHL RAX,{1}',
                        'MOV {0},RAX'
                        ),
                'RS':(
                        'MOV RAX,{0}',
                        'SHR RAX,{1}',
                        'MOV {0},RAX'
                        ),
                'AN':(
                        'MOV RAX,{0}',
                        'AND RAX,{1}',
                        'MOV {0},RAX'
                        ),
                'OR':(
                        'MOV RAX,{0}',
                        'OR RAX,{1}',
                        'MOV {0},RAX'
                        ),
                'RL':(
                        'MOV RAX,[{0}]',
                        'MOV {1},RAX'
                        ),
                'AA':(
                        'MOV RAX,{0}',
                        'ADD RAX,heapArray',
                        'MOV {1},RAX'
                        )
                }
        def generateCode(self,fullParse):
                codeString=(
                        #'FORMAT ELF64 EXECUTABLE\n'+
                        #'SEGMENT READABLE WRITABLE\n'+
                        'global _start\n'+
                        'section .data\n'+
                        ''.join(['{} DQ {}\n'.format(currReg, str(random.randint(0,2**64-1))) for currReg in globVars.registerNames])+
                        ''.join(['{} DQ {}\n'.format(currArr,fullParse['heapOffsets'][currArr]) for currArr in fullParse['heapOffsets'].keys()])+
                        'heapArray DB {}\n'.format(','.join([str(currByte) for currByte in fullParse['heapData']]))+
                        #'SEGMENT READABLE EXECUTABLE\n'+
                        #'ENTRY MQ_{}\n'.format(compOpts.entryPoint)+
                        'section .text\n'+
                        '_start: jmp MQ_{}\n'.format(compOpts.entryPoint)+
                        convertOps(self,fullParse['bodySection']))
                return codeString
        def compileFile(self):
                os.system('nasm {0}.Asm -felf64'.format(compOpts.outputName))
                os.system('ld -o {0} {0}.o'.format(compOpts.outputName))
        def execFile(self):
                os.system('./{}'.format(compOpts.outputName))
def convertOps(platForm,bodySection):
        convertedString = []
        for currElem in bodySection:
                if isinstance(currElem,labelT):
                        convertedString.append(currElem.labelName+':')
                elif isinstance(currElem,opT):
                        try:
                                opString = '{}{} {}\n'.format(platForm.commentChar,currElem.opCode,' '.join([str(currArg) for currArg in currElem.argsList]))+'\n'.join(
                                map(lambda currLine:'        '+currLine,platForm.opExpansion[currElem.opCode])).format(*currElem.argsList)
                        except KeyError:
                                printError('Operation \'{}\' has not yet been implemented on the platform \'{}\'.'.format(currElem.opCode,platForm.platFormName))
                        else:
                                convertedString.append(opString)
        return'\n'.join(convertedString)
def genFile(generatedCode):
        compFile = open(compOpts.outputName+'.Asm', 'w')
        compFile.write(generatedCode)
        compFile.close()
#
#Run the program
if __name__=='__main__':
        if len(sys.argv)<2:
                print 'Usage:\n        python2 {} (argumentFlags) (inputFiles)'.format(sys.argv[0])
                exit()
        inputFiles = parseArgs(sys.argv[1:])
        codeFromFiles = parseFiles(inputFiles)
        tokenLists = lexFiles(codeFromFiles)
        parsedFiles = parseSyntaxOfFiles(tokenLists,codeFromFiles)
        fullParse = parseHeap(parsedFiles)
        if compOpts.platForm == 'Linux':
                genAsm = genLinux
        generatedCode = genAsm().generateCode(fullParse)
        genFile(generatedCode)
        if compOpts.compFlags['-T'] == False:
                genAsm().compileFile()
                if compOpts.compFlags['-R'] == True:
                        genAsm().execFile()